import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ArrayOfPhones {
    ArrayList<Phone> phones = new ArrayList<>();

    public static void main(String[] args) {
        ArrayOfPhones arrayOfPhones = new ArrayOfPhones();
        arrayOfPhones.run();
    }

    private void run() {
        String songsRawData = getPhonesRawData();
        String[] rawStrings = songsRawData.split("\n");
        for (String rawString : rawStrings) {
            String[] split = rawString.split("/");
            phones.add(new Phone(split[0], split[1], split[2], split[3]));
        }
        Collections.sort(phones);
        System.out.println("phones by name");
        System.out.println(phones);
        Collections.sort(phones, new PhoneByMemoryComparator());
        System.out.println("phones by memory");
        System.out.println(phones);
        Collections.sort(phones, new PhoneByOverallSizeComparator());
        System.out.println("phones by overall size");
        System.out.println(phones);
    }

    private String getPhonesRawData(){
        return "Samsung/120/4/10\n" +
                "Apple/20/6/8\n" +
                "Xiaomi/20/7/5\n";
    }

    class PhoneByMemoryComparator implements Comparator<Phone> {
        public int compare(Phone o1, Phone o2) {
            return Integer.compare(Integer.parseInt(o1.getMemory()), Integer.parseInt(o2.getMemory()));
        }
    }

    class PhoneByOverallSizeComparator implements Comparator<Phone> {
        @Override
        public int compare(Phone o1, Phone o2) {
            int diagonal1 = Integer.parseInt(o1.getDiagonal());
            int thickness1 = Integer.parseInt(o1.getThickness());
            int diagonal2 = Integer.parseInt(o2.getDiagonal());
            int thickness2 = Integer.parseInt(o2.getThickness());
            int overallSize1 = diagonal1 * thickness1;
            int overallSize2 = diagonal2 * thickness2;
            return Integer.compare(overallSize1, overallSize2);
        }
    }
}

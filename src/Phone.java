public class Phone implements Comparable<Phone> {
    private String name;
    private String memory;
    private String diagonal;
    private String thickness;

    public Phone(String name, String memory, String diagonal, String thickness) {
        this.name = name;
        this.memory = memory;
        this.diagonal = diagonal;
        this.thickness = thickness;
    }

    public String getName() {
        return name;
    }

    public String getMemory() {
        return memory;
    }

    public String getDiagonal() {
        return diagonal;
    }

    public String getThickness() {
        return thickness;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "name='" + name + '\'' +
                ", memory=" + memory +
                ", diagonal=" + diagonal +
                ", thickness=" + thickness +
                '}';
    }

    @Override
    public int compareTo(Phone o) {
        return this.name.compareTo(o.getName());
    }
}